resource "google_compute_instance" "ubuntu-instance" {
  name         = "ubuntu-gcp"
  machine_type = "e2-medium"
  tags         = ["gitlab-runner"]

  boot_disk {
    initialize_params {
      image = "debian-11" # change it with "debian-11" when you run it for the first time
    }
  }

  network_interface {
    network = "default"

    access_config {
      nat_ip = google_compute_address.static_ip.address
    }
  }

  service_account {
    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    email  = var.email
    scopes = ["https://www.googleapis.com/auth/cloud-platform"]
  }

  scheduling {
    automatic_restart   = true // This ensures the VM is restarted automatically if it fails
    on_host_maintenance = "MIGRATE"
  }
}

## Enable the following after you build the machine and install the required configurations so you can create a snapshot and image from it

#  resource "google_compute_snapshot" "default" {
#   name        = "gitlab-runner-snapshot"
#   source_disk = google_compute_instance.ubuntu-instance.boot_disk.0.source
#  }

# resource "google_compute_image" "default" {
#   name            = "gitlab-runner-image"
#   source_snapshot = google_compute_snapshot.default.self_link
# }
