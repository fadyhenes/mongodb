variable "project" {
  default = "ageless-airship-402318"
}

variable "region" {
  default = "us-east1"
}

variable "zone" {
  default = "us-east1-b"
}

variable "email" {
  
  description = "the account service email is added to the tfvars"
}