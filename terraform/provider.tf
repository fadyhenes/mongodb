provider "google" {
  credentials = file("gcp-credentials.json") # Make sure your gcp credentails file is located with the terraform files the name is correct 
  project     = var.project
  region      = var.region
  zone        = var.zone
}
