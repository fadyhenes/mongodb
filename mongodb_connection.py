import json
import os
from pymongo import MongoClient

# Load the data to be inserted
with open('data.json', 'r') as file:
    document = json.load(file)

# Connect to the MongoDB instance
MONGODB_URI = os.environ.get('MONGODB_URI')
client = MongoClient(MONGODB_URI)


try:
    # Select (or create) the database 'surveyDB'
    db = client['Hospital-xyz']

    # Select (or create) the collection 'answers'
    collection = db['department-1']

    # Insert the document
    collection.insert_one(document)

    print("Document inserted successfully!")

except Exception as e:
    print(f"An error occurred: {e}")

finally:
    # Close the connection
    client.close()
