# Automate MongoDB using CI/CD

## Prerequisites

- Install [Terraform](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)
- Authenticate with [Google Cloud](https://cloud.google.com/sdk/gcloud/reference/auth/login)
- Build VM instance on GCP using Terraform `Terraform files already prepared in the terraform directory`
- Add the `Static-IP` in MongoDB IP Whitelist `To allow the VM instance to access MongoDB`
- Register GitLab Runner on the VM `gitlab runner`
- Configure the VM instance to allow access from GitLab.
- Ensure the required variables are added in GitLab CI/CD configuration.

## Build the VM instance

This will build the VM instance on GCP

```
cd terraform
terraform init
terraform apply --auto-approve
```

`NOTE: make sure to create and add the service account email in the terraform.tfvars file should look like this:`

```
email = "SERVICE_ACCOUNT@PROJECT_ID.iam.gserviceaccount.com"
```

`NOTE: Also make sure to download the gcp credentials json file by creating a key for the service account`

**make sure the gcp credentials file is added to `.gitignore` file and in the same terraform directory**

## IP Whitelist

Once the VM instance is created the output will show you the `Static-IP`. Navigate to `Mongodb Atlas > Network Access > Add IP Address` and add it there

## Optional

`The following steps already done on the used image by terraform, do it only if it's a clean VM`

To install GitLab Runner on a VM in Google Cloud Platform (GCP), follow these steps:

1. **Update the system packages:**

   First, update your system's package manager.

   ```sh
   sudo apt-get update
   ```

2. **Install the necessary dependencies:**

   Install `curl` if it's not already installed, as it will be used to download the GitLab Runner binary.

   ```sh
   sudo apt-get install -y curl
   ```

3. **Add the GitLab Runner repository and install the Runner:**

   Use the following script provided by GitLab to add their official repository and install the latest version of GitLab Runner:

   ```sh
   curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
   sudo apt-get install gitlab-runner
   ```

## Required

4. **Register the GitLab Runner:**

   After installation, you need to register the Runner with your GitLab instance.

   ```sh
   sudo gitlab-runner register
   ```

   During registration, you will be prompted for the following information:

   - **GitLab instance URL:** Enter the URL `https://gitlab.com/`.
   - **Registration token:** This token can be found in your project's settings under `Settings > CI/CD > Runners`. It's used to register the Runner with your GitLab instance.
   - **Description for the runner:** Enter a description for your Runner.
   - **Tags for the runner:** Enter tags for your Runner (optional, but useful for assigning specific runners to specific jobs).
   - **Executor:** Choose `shell` as an executor.

`NOTE: You can also use one command to register the Runner by doing the following:`

Navigate in your repository to `Settings > CI/CD > Runners` as shown in the image below.

<img src=runner.png>

Click on the three dots then choose `Show runner installation...`

<img src=runner-2.png>

You can use the command in the end of the page it should look like this:

```
sudo gitlab-runner register --url https://gitlab.com/ --registration-token TOKEN_HERE
```

**`IMPORTANT: Choose executor SHELL`**

After running the command you should see similar result:

<img src=runner-conf.png>

Once done you should see the VM online on your `Settings > CI/CD > Runner`

<img src=connected.png>

5. **Start and enable the GitLab Runner service:**

   Enable the Runner so it starts on boot, and start it immediately:

   ```sh
   sudo systemctl enable gitlab-runner
   sudo systemctl start gitlab-runner
   ```

6. **Verify the Runner is working:**

   Check the status of the GitLab Runner service to ensure it's running:

   ```sh
   sudo gitlab-runner status
   ```

   You can also check the Runners page in your GitLab project under `Settings > CI/CD` to see if the Runner you registered appears there.

### GitLab permissions:

Run the following to give permissions to GitLab to access the VM

```
sudo visudo
```

Then add the following line:

```
gitlab-runner ALL=(ALL) NOPASSWD:ALL
```

Now your GitLab Runner should be installed and registered to your GitLab instance. You can start using it to run your CI/CD jobs. Remember to configure your `.gitlab-ci.yml` file in your GitLab repository to define your CI/CD pipeline jobs.



### GitLab Variables:

Add MongoDB connection string to GitLab Variables.

<img src=GitLab-Variable.png>




===========================================

